﻿namespace VideoClubAviato
{
    partial class MainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainPage));
            this.labelShowMovies = new System.Windows.Forms.Label();
            this.labelShowActors = new System.Windows.Forms.Label();
            this.labelShowGenres = new System.Windows.Forms.Label();
            this.labelShowMovieRoles = new System.Windows.Forms.Label();
            this.labelShowUserCards = new System.Windows.Forms.Label();
            this.labelShowRentals = new System.Windows.Forms.Label();
            this.labelShowDirectors = new System.Windows.Forms.Label();
            this.pictureBoxDirectors = new System.Windows.Forms.PictureBox();
            this.pictureBoxMovies = new System.Windows.Forms.PictureBox();
            this.pictureBoxActors = new System.Windows.Forms.PictureBox();
            this.pictureBoxMovieRoles = new System.Windows.Forms.PictureBox();
            this.pictureBoxGenres = new System.Windows.Forms.PictureBox();
            this.pictureBoxUserCards = new System.Windows.Forms.PictureBox();
            this.pictureBoxHelp = new System.Windows.Forms.PictureBox();
            this.pictureBoxContact = new System.Windows.Forms.PictureBox();
            this.pictureBoxShowRentals = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDirectors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMovies)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxActors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMovieRoles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGenres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUserCards)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHelp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxContact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShowRentals)).BeginInit();
            this.SuspendLayout();
            // 
            // labelShowMovies
            // 
            this.labelShowMovies.AutoSize = true;
            this.labelShowMovies.Font = new System.Drawing.Font("Arial", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelShowMovies.Location = new System.Drawing.Point(576, 236);
            this.labelShowMovies.Name = "labelShowMovies";
            this.labelShowMovies.Size = new System.Drawing.Size(136, 40);
            this.labelShowMovies.TabIndex = 0;
            this.labelShowMovies.Text = "Filmovi";
            this.labelShowMovies.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelShowMovies.Click += new System.EventHandler(this.labelShowMovies_Click);
            this.labelShowMovies.MouseEnter += new System.EventHandler(this.labelShowMovies_MouseEnter);
            this.labelShowMovies.MouseLeave += new System.EventHandler(this.labelShowMovies_MouseLeave);
            // 
            // labelShowActors
            // 
            this.labelShowActors.AutoSize = true;
            this.labelShowActors.Font = new System.Drawing.Font("Arial", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelShowActors.Location = new System.Drawing.Point(573, 402);
            this.labelShowActors.Name = "labelShowActors";
            this.labelShowActors.Size = new System.Drawing.Size(131, 40);
            this.labelShowActors.TabIndex = 1;
            this.labelShowActors.Text = "Glumci";
            this.labelShowActors.Click += new System.EventHandler(this.labelShowActors_Click);
            this.labelShowActors.MouseEnter += new System.EventHandler(this.labelShowActors_MouseEnter);
            this.labelShowActors.MouseLeave += new System.EventHandler(this.labelShowActors_MouseLeave);
            // 
            // labelShowGenres
            // 
            this.labelShowGenres.AutoSize = true;
            this.labelShowGenres.Font = new System.Drawing.Font("Arial", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelShowGenres.Location = new System.Drawing.Point(573, 582);
            this.labelShowGenres.Name = "labelShowGenres";
            this.labelShowGenres.Size = new System.Drawing.Size(140, 40);
            this.labelShowGenres.TabIndex = 2;
            this.labelShowGenres.Text = "Zanrovi";
            this.labelShowGenres.Click += new System.EventHandler(this.labelShowGenres_Click);
            this.labelShowGenres.MouseEnter += new System.EventHandler(this.labelShowGenres_MouseEnter);
            this.labelShowGenres.MouseLeave += new System.EventHandler(this.labelShowGenres_MouseLeave);
            // 
            // labelShowMovieRoles
            // 
            this.labelShowMovieRoles.AutoSize = true;
            this.labelShowMovieRoles.Font = new System.Drawing.Font("Arial", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelShowMovieRoles.Location = new System.Drawing.Point(819, 402);
            this.labelShowMovieRoles.Name = "labelShowMovieRoles";
            this.labelShowMovieRoles.Size = new System.Drawing.Size(112, 40);
            this.labelShowMovieRoles.TabIndex = 3;
            this.labelShowMovieRoles.Text = "Uloge";
            this.labelShowMovieRoles.Click += new System.EventHandler(this.labelShowMovieRoles_Click);
            this.labelShowMovieRoles.MouseEnter += new System.EventHandler(this.labelShowMovieRoles_MouseEnter);
            this.labelShowMovieRoles.MouseLeave += new System.EventHandler(this.labelShowMovieRoles_MouseLeave);
            // 
            // labelShowUserCards
            // 
            this.labelShowUserCards.AutoSize = true;
            this.labelShowUserCards.Font = new System.Drawing.Font("Arial", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelShowUserCards.Location = new System.Drawing.Point(795, 582);
            this.labelShowUserCards.Name = "labelShowUserCards";
            this.labelShowUserCards.Size = new System.Drawing.Size(162, 40);
            this.labelShowUserCards.TabIndex = 4;
            this.labelShowUserCards.Text = "Korisnici";
            this.labelShowUserCards.Click += new System.EventHandler(this.labelShowUserCards_Click);
            this.labelShowUserCards.MouseEnter += new System.EventHandler(this.labelShowUserCards_MouseEnter);
            this.labelShowUserCards.MouseLeave += new System.EventHandler(this.labelShowUserCards_MouseLeave);
            // 
            // labelShowRentals
            // 
            this.labelShowRentals.AutoSize = true;
            this.labelShowRentals.Font = new System.Drawing.Font("Arial", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelShowRentals.Location = new System.Drawing.Point(371, 99);
            this.labelShowRentals.Name = "labelShowRentals";
            this.labelShowRentals.Size = new System.Drawing.Size(206, 40);
            this.labelShowRentals.TabIndex = 5;
            this.labelShowRentals.Text = "Rezervacije";
            this.labelShowRentals.Click += new System.EventHandler(this.labelShowRentals_Click);
            this.labelShowRentals.MouseEnter += new System.EventHandler(this.labelShowRentals_MouseEnter);
            this.labelShowRentals.MouseLeave += new System.EventHandler(this.labelShowRentals_MouseLeave);
            // 
            // labelShowDirectors
            // 
            this.labelShowDirectors.AutoSize = true;
            this.labelShowDirectors.Font = new System.Drawing.Font("Arial", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelShowDirectors.Location = new System.Drawing.Point(805, 236);
            this.labelShowDirectors.Name = "labelShowDirectors";
            this.labelShowDirectors.Size = new System.Drawing.Size(148, 40);
            this.labelShowDirectors.TabIndex = 6;
            this.labelShowDirectors.Text = "Reziseri";
            this.labelShowDirectors.Click += new System.EventHandler(this.labelShowDirectors_Click);
            this.labelShowDirectors.MouseEnter += new System.EventHandler(this.labelShowDirectors_MouseEnter);
            this.labelShowDirectors.MouseLeave += new System.EventHandler(this.labelShowDirectors_MouseLeave);
            // 
            // pictureBoxDirectors
            // 
            this.pictureBoxDirectors.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxDirectors.Image")));
            this.pictureBoxDirectors.Location = new System.Drawing.Point(819, 119);
            this.pictureBoxDirectors.Name = "pictureBoxDirectors";
            this.pictureBoxDirectors.Size = new System.Drawing.Size(120, 110);
            this.pictureBoxDirectors.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxDirectors.TabIndex = 7;
            this.pictureBoxDirectors.TabStop = false;
            this.pictureBoxDirectors.Click += new System.EventHandler(this.pictureBoxDirectors_Click);
            this.pictureBoxDirectors.MouseHover += new System.EventHandler(this.pictureBoxDirectors_MouseHover);
            // 
            // pictureBoxMovies
            // 
            this.pictureBoxMovies.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxMovies.Image")));
            this.pictureBoxMovies.Location = new System.Drawing.Point(566, 133);
            this.pictureBoxMovies.Name = "pictureBoxMovies";
            this.pictureBoxMovies.Size = new System.Drawing.Size(160, 111);
            this.pictureBoxMovies.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxMovies.TabIndex = 8;
            this.pictureBoxMovies.TabStop = false;
            this.pictureBoxMovies.Click += new System.EventHandler(this.pictureBoxMovies_Click);
            this.pictureBoxMovies.MouseHover += new System.EventHandler(this.pictureBoxMovies_MouseHover);
            // 
            // pictureBoxActors
            // 
            this.pictureBoxActors.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxActors.Image")));
            this.pictureBoxActors.Location = new System.Drawing.Point(566, 289);
            this.pictureBoxActors.Name = "pictureBoxActors";
            this.pictureBoxActors.Size = new System.Drawing.Size(147, 110);
            this.pictureBoxActors.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxActors.TabIndex = 9;
            this.pictureBoxActors.TabStop = false;
            this.pictureBoxActors.Click += new System.EventHandler(this.pictureBoxActors_Click);
            this.pictureBoxActors.MouseHover += new System.EventHandler(this.pictureBoxActors_MouseHover);
            // 
            // pictureBoxMovieRoles
            // 
            this.pictureBoxMovieRoles.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxMovieRoles.Image")));
            this.pictureBoxMovieRoles.Location = new System.Drawing.Point(815, 305);
            this.pictureBoxMovieRoles.Name = "pictureBoxMovieRoles";
            this.pictureBoxMovieRoles.Size = new System.Drawing.Size(120, 94);
            this.pictureBoxMovieRoles.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxMovieRoles.TabIndex = 10;
            this.pictureBoxMovieRoles.TabStop = false;
            this.pictureBoxMovieRoles.Click += new System.EventHandler(this.pictureBoxMovieRoles_Click);
            this.pictureBoxMovieRoles.MouseHover += new System.EventHandler(this.pictureBoxMovieRoles_MouseHover);
            // 
            // pictureBoxGenres
            // 
            this.pictureBoxGenres.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxGenres.Image")));
            this.pictureBoxGenres.Location = new System.Drawing.Point(582, 459);
            this.pictureBoxGenres.Name = "pictureBoxGenres";
            this.pictureBoxGenres.Size = new System.Drawing.Size(120, 110);
            this.pictureBoxGenres.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxGenres.TabIndex = 11;
            this.pictureBoxGenres.TabStop = false;
            this.pictureBoxGenres.Click += new System.EventHandler(this.pictureBoxGenres_Click);
            this.pictureBoxGenres.MouseHover += new System.EventHandler(this.pictureBoxGenres_MouseHover);
            // 
            // pictureBoxUserCards
            // 
            this.pictureBoxUserCards.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxUserCards.Image")));
            this.pictureBoxUserCards.Location = new System.Drawing.Point(798, 459);
            this.pictureBoxUserCards.Name = "pictureBoxUserCards";
            this.pictureBoxUserCards.Size = new System.Drawing.Size(152, 120);
            this.pictureBoxUserCards.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxUserCards.TabIndex = 12;
            this.pictureBoxUserCards.TabStop = false;
            this.pictureBoxUserCards.Click += new System.EventHandler(this.pictureBoxUserCards_Click);
            this.pictureBoxUserCards.MouseHover += new System.EventHandler(this.pictureBoxUserCards_MouseHover);
            // 
            // pictureBoxHelp
            // 
            this.pictureBoxHelp.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxHelp.Image")));
            this.pictureBoxHelp.Location = new System.Drawing.Point(872, 18);
            this.pictureBoxHelp.Name = "pictureBoxHelp";
            this.pictureBoxHelp.Size = new System.Drawing.Size(100, 50);
            this.pictureBoxHelp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHelp.TabIndex = 13;
            this.pictureBoxHelp.TabStop = false;
            this.pictureBoxHelp.Click += new System.EventHandler(this.pictureBoxHelp_Click);
            this.pictureBoxHelp.MouseHover += new System.EventHandler(this.pictureBoxHelp_MouseHover);
            // 
            // pictureBoxContact
            // 
            this.pictureBoxContact.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxContact.Image")));
            this.pictureBoxContact.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxContact.Name = "pictureBoxContact";
            this.pictureBoxContact.Size = new System.Drawing.Size(80, 70);
            this.pictureBoxContact.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxContact.TabIndex = 14;
            this.pictureBoxContact.TabStop = false;
            this.pictureBoxContact.Click += new System.EventHandler(this.pictureBoxContact_Click);
            this.pictureBoxContact.MouseHover += new System.EventHandler(this.pictureBoxContact_MouseHover);
            // 
            // pictureBoxShowRentals
            // 
            this.pictureBoxShowRentals.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxShowRentals.Image")));
            this.pictureBoxShowRentals.Location = new System.Drawing.Point(378, 18);
            this.pictureBoxShowRentals.Name = "pictureBoxShowRentals";
            this.pictureBoxShowRentals.Size = new System.Drawing.Size(187, 78);
            this.pictureBoxShowRentals.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxShowRentals.TabIndex = 17;
            this.pictureBoxShowRentals.TabStop = false;
            this.pictureBoxShowRentals.Click += new System.EventHandler(this.pictureBoxShowRentals_Click);
            this.pictureBoxShowRentals.MouseHover += new System.EventHandler(this.pictureBoxShowRentals_MouseHover);
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(984, 661);
            this.Controls.Add(this.pictureBoxShowRentals);
            this.Controls.Add(this.pictureBoxContact);
            this.Controls.Add(this.pictureBoxHelp);
            this.Controls.Add(this.pictureBoxUserCards);
            this.Controls.Add(this.pictureBoxGenres);
            this.Controls.Add(this.pictureBoxMovieRoles);
            this.Controls.Add(this.pictureBoxActors);
            this.Controls.Add(this.pictureBoxMovies);
            this.Controls.Add(this.pictureBoxDirectors);
            this.Controls.Add(this.labelShowDirectors);
            this.Controls.Add(this.labelShowRentals);
            this.Controls.Add(this.labelShowUserCards);
            this.Controls.Add(this.labelShowMovieRoles);
            this.Controls.Add(this.labelShowGenres);
            this.Controls.Add(this.labelShowActors);
            this.Controls.Add(this.labelShowMovies);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1000, 700);
            this.MinimumSize = new System.Drawing.Size(1000, 700);
            this.Name = "MainPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainPage";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDirectors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMovies)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxActors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMovieRoles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGenres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUserCards)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHelp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxContact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShowRentals)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelShowMovies;
        private System.Windows.Forms.Label labelShowActors;
        private System.Windows.Forms.Label labelShowGenres;
        private System.Windows.Forms.Label labelShowMovieRoles;
        private System.Windows.Forms.Label labelShowUserCards;
        private System.Windows.Forms.Label labelShowRentals;
        private System.Windows.Forms.Label labelShowDirectors;
        private System.Windows.Forms.PictureBox pictureBoxDirectors;
        private System.Windows.Forms.PictureBox pictureBoxMovies;
        private System.Windows.Forms.PictureBox pictureBoxActors;
        private System.Windows.Forms.PictureBox pictureBoxMovieRoles;
        private System.Windows.Forms.PictureBox pictureBoxGenres;
        private System.Windows.Forms.PictureBox pictureBoxUserCards;
        private System.Windows.Forms.PictureBox pictureBoxHelp;
        private System.Windows.Forms.PictureBox pictureBoxContact;
        private System.Windows.Forms.PictureBox pictureBoxShowRentals;
    }
}